class gitolite($ensure=file, $gituser='git', $admin_key, $path="/var/lib/$gituser") {

  case $::operatingsystem {
    'debian': {
      file { '/var/cache/debconf/gitolite.preseed':
        ensure  => $ensure,
        content => template('gitolite/gitolite.preseed.erb'),
        before  => Package['gitolite'],
      }
    }
    default: { fail "awfully sorry, but ${::operatingsystem} is not yet supported" }
  }

  package { 'gitolite':
    ensure       => present,
    responsefile => '/var/cache/debconf/gitolite.preseed',
  }

  file {
    '/var/lib/gitolite':
      mode    => '0750',
      owner   => gitolite,
      group   => gitolite,
      require => Package[gitolite];

    '/var/lib/gitolite/repositories':
      mode  => '0770',
      owner => gitolite,
      group => gitolite;

    '/var/lib/gitolite/.gitolite.rc':
      source => [ 'puppet:///modules/site_gitolite/gitolite.rc',
                  'puppet:///modules/gitolite/gitolite.rc' ],
      mode   => '0644',
      owner  => gitolite,
      group  => gitolite;

    '/usr/share/doc/git-core/contrib/hooks/post-receive-email':
      mode   => '0755',
      owner  => root,
      group  => root;

    '/var/lib/gitolite/.gitolite':
      ensure  => directory,
      mode    => '0700',
      owner   => gitolite,
      group   => gitolite,
      require => Package['gitolite'],
      notify  => Exec['gl-setup'];
  }

  exec { 'gl-setup':
    command     => '/usr/bin/gl-setup',
    refreshonly => true;
  }
}
